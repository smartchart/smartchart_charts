# smartchart_charts

#### 介绍
用于smartchart的图形开发, 用于对echarts到smartchart的图形转化

#### 注意事项

1.  转化来源 [Echarts样列](https://echarts.apache.org/examples/zh/index.html)
2.  目录和文件名请使用echarts官方样列一样的名称
3.  具体的使用方法请参考项目 [Smartchart](https://gitee.com/smartchart/smartchart)

#### 参与贡献

你可以fork提交, 也可以直接网页编辑提交